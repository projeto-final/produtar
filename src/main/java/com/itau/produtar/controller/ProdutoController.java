package com.itau.produtar.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.produtar.model.Produto;
import com.itau.produtar.repository.ProdutoRepository;

@Controller
public class ProdutoController {
	@Autowired
	ProdutoRepository produtoRepository;
	
	@RequestMapping(path="/produtos/{idade}", method=RequestMethod.GET)
	@ResponseBody
	public Set<Produto> getProdutosByIdade(@PathVariable int idade) {	
		
		return produtoRepository.findByIdade(idade);
	}
	
}
