package com.itau.produtar.repository;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.itau.produtar.model.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, Integer>{
	Set<Produto> findByIdade(int idade);
}
